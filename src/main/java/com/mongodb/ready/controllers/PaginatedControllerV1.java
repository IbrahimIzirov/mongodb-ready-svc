package com.mongodb.ready.controllers;

import com.mongodb.ready.models.PaginatedRequest;
import com.mongodb.ready.services.PaginatedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1")
public class PaginatedControllerV1 {

    private final PaginatedService paginatedService;

    @Autowired
    public PaginatedControllerV1(PaginatedService paginatedService) {
        this.paginatedService = paginatedService;
    }

    @GetMapping("/paginations")
    public List<PaginatedRequest> getAll() {
        return paginatedService.getAll();
    }

    @GetMapping("/pagination")
    public PaginatedRequest getBySource(@RequestParam String source) {
        return paginatedService.getBySource(source);
    }

    @PostMapping("/pageable/save")
    public PaginatedRequest save(@Valid @RequestBody PaginatedRequest request) {
        return paginatedService.save(request);
    }
}