package com.mongodb.ready.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Main object for paginated operation.
 * Here @Document annotation is for collection name in mongo database.
 *
 * @author Ibrahim Izirov
 * @since 01-12-2022
 */
@Document("paginated_collection")
public class PaginatedRequest {

    @Id
    private String id;

    @NotNull
    private long desiredPage;

    @NotNull
    private long maxRows;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss.SS")
    private LocalDateTime startDate;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss.SS")
    private LocalDateTime endDate;

    private String source;

    private FilterData filterData;
    private OrderData orderData;

    public PaginatedRequest() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getDesiredPage() {
        return desiredPage;
    }

    public void setDesiredPage(long desiredPage) {
        this.desiredPage = desiredPage;
    }

    public long getMaxRows() {
        return maxRows;
    }

    public void setMaxRows(long maxRows) {
        this.maxRows = maxRows;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public FilterData getFilterData() {
        return filterData;
    }

    public void setFilterData(FilterData filterData) {
        this.filterData = filterData;
    }

    public OrderData getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderData orderData) {
        this.orderData = orderData;
    }
}
