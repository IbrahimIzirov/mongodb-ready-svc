package com.mongodb.ready.services.impl;

import com.mongodb.ready.models.PaginatedRequest;
import com.mongodb.ready.repositories.CustomPaginatedRepository;
import com.mongodb.ready.repositories.PaginatedJPARepository;
import com.mongodb.ready.services.PaginatedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaginatedServiceImpl implements PaginatedService {

    private final PaginatedJPARepository jpaRepository;
    private final CustomPaginatedRepository customPaginatedRepository;

    @Autowired
    public PaginatedServiceImpl(PaginatedJPARepository jpaRepository,
                                CustomPaginatedRepository customPaginatedRepository) {
        this.jpaRepository = jpaRepository;
        this.customPaginatedRepository = customPaginatedRepository;
    }

    @Override
    public List<PaginatedRequest> getAll() {
        return jpaRepository.findAll();
    }

    @Override
    public PaginatedRequest getBySource(String source) {
        return customPaginatedRepository.getBySource(source);
    }

    @Override
    public PaginatedRequest save(PaginatedRequest request) {
        return jpaRepository.save(request);
    }
}