package com.mongodb.ready.services;

import com.mongodb.ready.models.PaginatedRequest;

import java.util.List;

/**
 * Service with crud operations.
 *
 * @author Ibrahim Izirov
 * @since 01-12-2022
 */
public interface PaginatedService {

    List<PaginatedRequest> getAll();

    PaginatedRequest getBySource(String source);

    PaginatedRequest save(PaginatedRequest request);
}