package com.mongodb.ready.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * A configuration file that reads settings from the yml file and creates beans to be controlled by Hibernate.
 *
 * @author Ibrahim Izirov
 * @since 01-12-2022
 */
@Configuration
@PropertySource("classpath:application.yml")
public class DatabaseConfig {

    private final String username, password, database;
    private final String port, host;

    @Autowired
    public DatabaseConfig(Environment env) {
        username = env.getProperty("mongodb.username");
        password = env.getProperty("mongodb.password");
        database = env.getProperty("mongodb.database");
        port = env.getProperty("mongodb.port");
        host = env.getProperty("mongodb.host");
    }

    @Bean
    public MongoClient mongo() {
        ConnectionString connectionString = new ConnectionString(
                String.format("mongodb://%s:%s@%s:%s/?authSource=%s", username, password, host, port, database));
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();

        return MongoClients.create(mongoClientSettings);
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongo(), database);
    }
}