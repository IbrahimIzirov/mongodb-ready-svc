package com.mongodb.ready.repositories.impl;

import com.mongodb.ready.models.PaginatedRequest;
import com.mongodb.ready.repositories.CustomPaginatedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomPaginatedRepositoryImpl implements CustomPaginatedRepository {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public CustomPaginatedRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public PaginatedRequest getBySource(String source) {
        Query query = new Query();
        query.addCriteria(Criteria.where("source").is(source));
        List<PaginatedRequest> list = mongoTemplate.find(query, PaginatedRequest.class);
        return list.get(0);
    }
}