package com.mongodb.ready.repositories;

import com.mongodb.ready.models.PaginatedRequest;

/**
 * Custom Repository for access mongo database.
 *
 * @author Ibrahim Izirov
 * @since 01-12-2022
 */
public interface CustomPaginatedRepository {

    /**
     * Get object by source.
     *
     * @param source - source name
     */
    PaginatedRequest getBySource(String source);
}