package com.mongodb.ready.repositories;

import com.mongodb.ready.models.PaginatedRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository with crud operations.
 *
 * @author Ibrahim Izirov
 * @since 01-12-2022
 */
@Repository
public interface PaginatedJPARepository extends MongoRepository<PaginatedRequest, String> {
}